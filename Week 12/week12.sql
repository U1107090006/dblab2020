explain select * from proteins where accession = "Q11130" ;
explain select * from proteins where pid = "GAG2A_HUMAN" ;
explain select * from proteins where pid like "%HUMAN" ; 
#like operatörü index olmasından
#cok etkilenmiyor çünkü yinede hepsine bakıyor row sayısından da belli oluyor zaten
#bu yüzden like operatörü kullanmak bu durumlarda cok kullanıslı değildir

alter table proteins drop index idx2 ;

create index idx1 on proteins(accession);
alter table proteins drop index idx1 ;

create index idx3 on proteins(pid) ;
alter table proteins drop index idx3 ;

select count(pid) from proteins where pid like "%HUMAN" ;

select * from proteins where pid like "%HUMAN" ;
