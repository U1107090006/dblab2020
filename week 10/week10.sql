CREATE DEFINER=`root`@`localhost` PROCEDURE `Counter`(INOUT cnt INT , IN inc INT)
BEGIN
	set cnt = cnt + inc ;
END
CREATE DEFINER=`root`@`localhost` PROCEDURE `SelectCustomersByCity`(IN c varchar(45))
BEGIN
	select *
    from customers
    where City = c ;
    set c = "London" ;
END
CREATE DEFINER=`root`@`localhost` PROCEDURE `SelectAllCustomers`()
BEGIN
	select * from customers ;
END
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetOrderCountByShipper`(in shipper varchar(45) , out count int)
BEGIN
	select count(OrderID) into count
	from orders join shippers on orders.ShipperID = shippers.ShipperID
	where shipperName = shipper ;
END


call company.SelectAllCustomers();
set @city = "Madrid" ;
call company.SelectCustomersByCity(@city);
select @city ;

select count(OrderID)
from orders join shippers on orders.ShipperID = shippers.ShipperID
where shipperName = "United Package" ;

call GetOrderCountByShipper("United Package",@count) ;
select @count ;

set@counter = 1 ;
call Counter(@counter , 1);
select @counter ;